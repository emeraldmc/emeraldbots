package fr.tristiisch.emeraldmc.bots.teamspeak;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.bots.Main;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TeamspeakCommand extends Command {

	private static Map<UUID, Long> cooldown = new HashMap<>();

	public static void addPlayer(final ProxiedPlayer player) {
		cooldown.put(player.getUniqueId(), Utils.getCurrentTimeinSeconds() + 60);
	}

	public static boolean canUse(final ProxiedPlayer player) {
		if(cooldown.containsKey(player.getUniqueId())) {
			if(cooldown.get(player.getUniqueId()) < Utils.getCurrentTimeinSeconds()) {
				cooldown.remove(player.getUniqueId());
				return true;
			}
			return false;
		}
		return true;
	}

	public static void remove(final ProxiedPlayer player) {
		cooldown.remove(player.getUniqueId());
	}

	public static void sendTeamspeakIp(final ProxiedPlayer player) {
		final TextComponent message = new TextComponent(Utils.color("&2EmeraldMC &7» &aTeamspeak: "));

		final TextComponent ip = new TextComponent(Utils.color("&2ts.emeraldmc.fr"));
		ip.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&2Cliquez pour copier l'IP")).create()));
		ip.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "ts.emeraldmc.fr"));
		message.addExtra(ip);
		player.sendMessage(message);
	}

	public TeamspeakCommand(final String command, final String... aliases) {
		super(command, null, aliases);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(Main.getInstance(), () -> {
			ProxiedPlayer player = null;
			if(sender instanceof ProxiedPlayer) {
				player = (ProxiedPlayer) sender;
			}

			if(args.length == 0) {
				if(player == null) {
					sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
					return;
				}
				sendTeamspeakIp(player);

			} else if(args[0].equalsIgnoreCase("link")) {
				if(player == null) {
					sender.sendMessage(BungeeConfigUtils.getString("commun.messages.cantconsole"));
					return;
				}
				if(!canUse(player)) {
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &cMerci de patienter avant de faire &4/ts link&c."));
					return;
				}
				TeamspeakUtils.check(player);

			} else if(args[0].equalsIgnoreCase("stop")) {

				if(player != null) {
					final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
					if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.RESPMODO)) {
						sender.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
						return;
					}
				}

				TeamspeakBot.disconnect();
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &cTeamspeakBot deconnecté."));

			} else if(args[0].equalsIgnoreCase("start")) {

				if(player != null) {
					final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
					if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.RESPMODO)) {
						sender.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
						return;
					}
				}
				TeamspeakBot.connect();
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &aTeamspeakBot connecté."));
			}
		});
	}
}

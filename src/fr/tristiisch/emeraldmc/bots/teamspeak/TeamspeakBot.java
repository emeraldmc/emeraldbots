package fr.tristiisch.emeraldmc.bots.teamspeak;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Collectors;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.bots.Main;
import net.md_5.bungee.api.ProxyServer;

public class TeamspeakBot {


	private static String name = EmeraldBungee.getName();

	static TS3Query queryConfig;
	static TS3Api query;


	static List<Integer> helpchannelsId;
	static List<Integer> helpchannelsIdAdmin;

	public static void connect() {
		final TS3Config config = new TS3Config();
		config.setHost("ts.emeraldmc.fr");

		queryConfig = new TS3Query(config);
		try {
			queryConfig.connect();
		} catch(final Exception e) {
			ProxyServer.getInstance().getScheduler().schedule(Main.getInstance(), () -> connect(), 60, TimeUnit.SECONDS);
		}

		query = queryConfig.getApi();
		query.login("serveradmin", "GmCIiKV0");
		query.selectVirtualServerById(1);

		for(final Client client : query.getClients()) {
			if(client.getNickname().equalsIgnoreCase(name)) {
				if(client.isServerQueryClient()) {
					EmeraldBungee.getInstance().getLogger().log(Level.SEVERE, "Un ServerQuery nommé " + name + " est déjà connecter.");
					disconnect();
					return;
				} else {
					query.kickClientFromServer("Merci de ne pas utiliser le nom '" + name + "'.", client);
				}
			}
		}
		query.setNickname(name);

		final int defaultServerGroupID = query.getInstanceInfo().getDefaultServerGroup();
		for(final ServerGroup group : query.getServerGroups()) {
			for(final EmeraldGroup emeraldgroup : EmeraldGroup.values()) {
				if(group.getId() == emeraldgroup.getTS3ID()) {
					if(emeraldgroup.isStaffMember()) {
						TeamspeakUtils.minecraftStaffGroup.add(group.getId());
					}
					TeamspeakUtils.minecraftGroup.put(group.getId(), group);
				}
			}
			if(group.getId() == defaultServerGroupID) {
				TeamspeakUtils.defaultServerGroup = group;
			}
		}

		helpchannelsId = query.getChannelsByName("Besoin d'aide #").stream().filter(c -> c.isPermanent()).map(c -> c.getId()).collect(Collectors.toList());
		helpchannelsIdAdmin = query.getChannelsByName("Besoin d'un Admin").stream().filter(c -> c.isPermanent()).map(c -> c.getId()).collect(Collectors.toList());
		/*
		 * List<Integer> helpchannelsID = new ArrayList<>(); // Besoin d'aide channels
		 * helpchannelsID.add(18); helpchannelsID.add(16); helpchannelsID.add(15);
		 */

		query.registerAllEvents();
		query.addTS3Listeners(new TS3EventAdapter() {

			@Override
			public void onTextMessage(final TextMessageEvent event) {
				final int clientID = event.getInvokerId();
				final ClientInfo client = query.getClientInfo(clientID);
				if(event.getTargetMode() == TextMessageTargetMode.CLIENT && !client.isServerQueryClient()) {
					final String message = event.getMessage().toLowerCase();

					if(message.startsWith("salut")) {
						query.sendPrivateMessage(clientID, "Bonjour " + event.getInvokerName() + "!");
					}
				}
			}

			@Override
			public void onClientJoin(final ClientJoinEvent event) {
				final ClientInfo client = query.getClientInfo(event.getClientId());

				if(!client.isServerQueryClient()) {
					final int[] clientgroupsid = client.getServerGroups();

					final List<Integer> ClientMinecraftGroup = Arrays.stream(clientgroupsid).filter(clientgroupid -> TeamspeakUtils.minecraftGroup.containsKey(clientgroupid)).boxed().collect(
							Collectors.toList());

					if(ClientMinecraftGroup.size() > 1) {
						TeamspeakUtils.removeAllServerGroup(client);
						query.sendPrivateMessage(event.getClientId(), "Il est impossible d'avoir plusieurs grades sur le teamspeak.");
					}

					final EmeraldPlayer emeraldplayer = MySQL.getPlayerByTS3ID(client.getDatabaseId());
					if(emeraldplayer == null) {
						query.editClient(client.getId(), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, ""));
						query.pokeClient(event.getClientId(), "Pour avoir accès à toutes les fonctionnalités, faites '/ts link' sur le serveur Minecraft.");
						return;
					}

					if(!client.getNickname().equals(emeraldplayer.getName())) {
						query.kickClientFromServer("Il est interdit de changer votre pseudo. Merci de garder '" + emeraldplayer.getName() + "'.", client);
					}

					TeamspeakUtils.updateRank(emeraldplayer, client);
				}
			}

			@Override
			public void onClientMoved(final ClientMovedEvent event) {
				final int channelID = event.getTargetChannelId();

				final ChannelInfo channelInfo = query.getChannelInfo(channelID);
				final Channel channel = query.getChannelByNameExact(channelInfo.getName(), false);
				if(channel.getTotalClients() != 1) {
					return;
				}

				if(TeamspeakBot.helpchannelsId.contains(channelID)) {
					final int clientID = event.getClientId();
					final ClientInfo clientInfo = query.getClientInfo(clientID);
					int i = 0;
					for(final Client staff : query.getClients()) {
						if(TeamspeakUtils.isInGroup(staff, EmeraldGroup.MODERATEUR, EmeraldGroup.GUIDE, EmeraldGroup.RESPMODO) && !TeamspeakUtils.isInGroup(staff, 40)) {
							query.sendPrivateMessage(staff.getId(),
									TeamspeakUtils.getClientURI(clientInfo) + "[color=green] a besoin de l'aide d'un Modérateur/Guide dans le channel [/color]" + TeamspeakUtils
									.getChannelURI(channelInfo));
							i++;
						}
					}
					if(i == 0) {
						query.kickClientFromChannel("[color=green]Aucun Guide ou Modérateur n'est actuellement disponible, merci de réésayer plus tard.[/color]", clientID);
						return;
					}

					query.pokeClient(clientID, "[color=green]Merci de patienter l'arrivée d'un Guide ou Modérateur.[/color]");
				} else if(TeamspeakBot.helpchannelsIdAdmin.contains(channelID)) {
					final int clientID = event.getClientId();
					final ClientInfo clientInfo = query.getClientInfo(clientID);
					int i = 0;
					for(final Client staff : query.getClients()) {
						if(TeamspeakUtils.isInGroup(staff, EmeraldGroup.ADMIN) && !TeamspeakUtils.isInGroup(staff, 40)) {
							query.sendPrivateMessage(staff.getId(),
									TeamspeakUtils.getClientURI(clientInfo) + "[color=red] a besoin de l'aide d'un Admin dans le channel [/color]" + TeamspeakUtils.getChannelURI(channelInfo));
							i++;
						}
					}
					if(i == 0) {
						query.pokeClient(clientID, "[color=red]Aucun Admin n'est actuellement disponible, merci de patienter.[/color]");
						return;
					}

					query.pokeClient(clientID, "[color=red]Merci de patienter l'arrivée d'un Administrateur.[/color]");
				}
			}

		});
	}

	public static void disconnect() {
		if(query != null) {
			query.unregisterAllEvents();
			queryConfig.exit();
			query.logout();
		}
	}

	// ############################# methods



}

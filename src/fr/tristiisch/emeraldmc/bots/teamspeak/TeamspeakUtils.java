package fr.tristiisch.emeraldmc.bots.teamspeak;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import com.google.common.primitives.Ints;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class TeamspeakUtils {

	static ServerGroup defaultServerGroup = null;
	static Map<Integer, ServerGroup> minecraftGroup = new HashMap<>();
	static List<Integer> minecraftStaffGroup = new ArrayList<>();

	@SuppressWarnings("deprecation")
	public static void check(final ProxiedPlayer player) {

		if(TeamspeakBot.query == null) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cLa laison teamspeak/serveur s'est cassé."));
			return;
		}
		final List<Client> sameip = TeamspeakBot.query.getClients().stream().filter(client -> client.getIp().equals(player.getAddress().getAddress().getHostAddress())).collect(Collectors.toList());
		if(sameip.isEmpty()) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez être connecté sur le Teamspeak."));
			return;
		}

		final Client cl = sameip.stream().filter(client -> client.getNickname().equalsIgnoreCase(player.getName())).findFirst().orElse(null);

		if(cl == null) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez utiliser le même pseudo sur Minecraft & sur Teamspeak !"));
			return;
		}
		setSynchronized(player, cl);
	}

	public static String getChannelURI(final ChannelInfo channel) {
		return "[URL=channelid://" + channel.getId() + "]" + channel.getName() + "[/URL]";
	}

	public static String getClientURI(final Client client) {
		return "[URL=" + client.getClientURI() + "]" + client.getNickname() + "[/URL]";
	}

	public static boolean isInGroup(final Client client, final EmeraldGroup... groups) {
		return Arrays.stream(client.getServerGroups()).filter(s -> Arrays.stream(groups).map(EmeraldGroup::getTS3ID).filter(gId -> gId == s).findFirst().isPresent()).findFirst().isPresent();
	}

	public static boolean isInGroup(final Client client, final int groupID) {
		return Arrays.stream(client.getServerGroups()).filter(s -> groupID == s).findFirst().isPresent();
	}

	public static boolean isStaff(final Client client) {
		return Arrays.stream(client.getServerGroups()).filter(s -> minecraftStaffGroup.contains(s)).findFirst().isPresent();
	}

	public static void removeAllServerGroup(final Client client) {

		for(final int clientservergroupid : client.getServerGroups()) {
			TeamspeakBot.query.removeClientFromServerGroup(clientservergroupid, client.getDatabaseId());
		}
	}

	@SuppressWarnings("deprecation")
	public static void setSynchronized(final ProxiedPlayer player, final Client client) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
			final EmeraldPlayer emeraldPlayer = accountProvider.getEmeraldPlayer();

			TeamspeakBot.query.editClient(client.getId(), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Pseudo Minecraft: " + player.getName()));
			removeAllServerGroup(client);

			TeamspeakBot.query.addClientToServerGroup(emeraldPlayer.getGroup().getTS3ID(), client.getDatabaseId());

			emeraldPlayer.setTS3ID(client.getDatabaseId());
			accountProvider.sendAccountToRedis(emeraldPlayer);
			TeamspeakBot.query.sendPrivateMessage(client.getId(), "Votre indentité Teamspeak est désormais liée au compte Minecraft '" + player.getName() + "'.");
			player.sendMessage(Utils.color("&2EmeraldMC &7» &aVotre compte Minecraft est désormais liée à votre identité Teamspeak '" + client.getNickname() + "'."));
		});
	}

	public static void updateRank(final EmeraldPlayer emeraldPlayer, final ClientInfo client) {

		TeamspeakBot.query.editClient(client.getId(), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Pseudo Minecraft: " + emeraldPlayer.getName()));
		if(!Ints.asList(client.getServerGroups()).contains(emeraldPlayer.getGroup().getTS3ID())) {
			removeAllServerGroup(client);
		}

		TeamspeakBot.query.addClientToServerGroup(emeraldPlayer.getGroup().getTS3ID(), client.getDatabaseId());

		emeraldPlayer.setTS3ID(client.getDatabaseId());
	}
}

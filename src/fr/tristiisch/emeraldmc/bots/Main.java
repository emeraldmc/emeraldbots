package fr.tristiisch.emeraldmc.bots;

import fr.tristiisch.emeraldmc.bots.discord.DiscordCommand;
import fr.tristiisch.emeraldmc.bots.discord.EmeraldDiscord;
import fr.tristiisch.emeraldmc.bots.teamspeak.TeamspeakBot;
import fr.tristiisch.emeraldmc.bots.teamspeak.TeamspeakCommand;
import fr.tristiisch.emeraldmc.bots.teamspeak.TeamspeakListener;
import fr.tristiisch.emeraldmc.bots.twitter.TwitterAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public class Main extends Plugin {

	private static Plugin instance;

	public static Plugin getInstance() {
		return instance;
	}

	@Override
	public void onDisable() {
		EmeraldDiscord.disconnect();
		TwitterAPI.disconnect();
		TeamspeakBot.disconnect();
		System.out.println(ChatColor.RED + this.getDescription().getName() + " est désactivé.");
	}

	@Override
	public void onEnable() {
		instance = this;

		final PluginManager pluginmanager = this.getProxy().getPluginManager();
		pluginmanager.registerListener(this, new TeamspeakListener());

		pluginmanager.registerCommand(this, new TeamspeakCommand("ts", "teamspeak", "ts3"));
		pluginmanager.registerCommand(this, new DiscordCommand("discord"));

		this.getProxy().getScheduler().runAsync(this, () -> {
			EmeraldDiscord.connect();
			TeamspeakBot.connect();
			TwitterAPI.connect();
		});
		System.out.println(ChatColor.GREEN + this.getDescription().getName() + " est activé.");
	}
}

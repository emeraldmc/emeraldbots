package fr.tristiisch.emeraldmc.bots.discord;

import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.SelfUser;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberNickChangeEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.md_5.bungee.api.ProxyServer;

public class DiscordListener extends ListenerAdapter {

	@Override
	public void onGuildJoin(final GuildJoinEvent event) {
		final Guild guild = event.getGuild();
		final String botName = EmeraldDiscord.jda.getSelfUser().getName();

		if(guild.getOwner().getUser().getIdLong() != 177524565373616128l) {

			final User owner = guild.getOwner().getUser();
			ProxyServer.getInstance().getLogger().log(Level.SEVERE,
					botName + " s'est déconnecté du discord " + guild.getName() + " car ce n'est pas le discord officiel (" + owner.getName() + "#" + owner.getDiscriminator());
			event.getGuild().leave();
			return;
		}
		ProxyServer.getInstance().getLogger().log(Level.INFO, botName + " est connecté du discord " + guild.getName());
		for(final Member member : guild.getMembers()) {
			EmeraldDiscord.checkName(event.getGuild(), member);
		}
	}

	@Override
	public void onGuildMemberJoin(final GuildMemberJoinEvent event) {
		EmeraldDiscord.checkName(event.getGuild(), event.getMember());
	}

	@Override
	public void onGuildMemberNickChange(final GuildMemberNickChangeEvent event) {
		EmeraldDiscord.checkName(event.getGuild(), event.getMember());
	}

	@Override
	public void onMessageReceived(final MessageReceivedEvent event) {

		final Guild guild = event.getGuild();
		final Message message = event.getMessage();
		final MessageChannel channel = event.getChannel();
		final String msg = message.getContentDisplay();

		if(msg.startsWith("!rank")) {
			message.delete().queue();
			final Member member = event.getMember();
			final String[] args = message.getContentDisplay().split(" ");
			if(args.length != 2) {
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ Usage: !rank <pseudo>");
				return;
			}

			final EmeraldPlayer emeraldPlayer = MySQL.getPlayer(args[1]);
			if(emeraldPlayer == null) {
				EmeraldDiscord.sendTempMessageToChannel(channel, "Le pseudo " + args[1] + " est associé à aucun compte EmeraldMC.");
				return;
			}
			EmeraldDiscord.sendTempMessageToChannel(channel, "Le joueur " + emeraldPlayer.getName() + " a le grade " + emeraldPlayer.getGroup().getName() + " sur EmeraldMC.");


		} else if(msg.startsWith("!setrank")) {
			message.delete().queue();

			final Member member = event.getMember();
			if(!member.hasPermission(Permission.MANAGE_ROLES)) {
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ Vous n'avez pas la permission d'utiliser !setrank.");
			}
			final String[] args = message.getContentDisplay().split(" ");
			if(args.length != 2) {
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ Usage: !setrank <pseudo>");
				return;
			}
			EmeraldDiscord.setRole(member, event.getGuild(), channel, args[1]);

		} else if(msg.startsWith("!check")) {
			message.delete().queue();
			final Member member = event.getMember();
			if(!member.hasPermission(Permission.MANAGE_ROLES)) {
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ Vous n'avez pas la permission d'utiliser !check.");
				return;
			}
			final String[] args = message.getContentDisplay().split(" ");
			if(args.length < 1) {
				return;
			}
			final TextChannel targetChannel = guild.getTextChannelsByName(args[1], true).get(0);
			/* final long msgid = targetChannel.getLatestMessageIdLong(); */
			final Message lastmsg = targetChannel.getPinnedMessages().complete().get(0);

			final MessageBuilder out = new MessageBuilder(event.getAuthor().getAsMention());
			out.append(" Voici les réactions au msg '");
			out.append(lastmsg.getContentRaw());
			out.append("' de '");
			out.append(lastmsg.getAuthor().getName());
			out.append("': \n");
			out.append(lastmsg.getReactions().size());
			out.append(lastmsg.getReactions()
					.stream()
					.map(reac -> reac.getReactionEmote().getEmote().getName() + ": " + reac.getUsers().complete().stream().map(User::getName).collect(Collectors.joining(", ")))
					.collect(Collectors.joining("\n")));
			channel.sendMessage(out.build()).queue();

		} else if(msg.startsWith("!instance")) {
			message.delete().queue();
			final Member member = event.getMember();
			if(!member.hasPermission(Permission.MANAGE_ROLES)) {
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ Vous n'avez pas la permission d'utiliser !instance.");
			}
			final JDA jda = event.getJDA();
			final SelfUser user = jda.getSelfUser();

			EmeraldDiscord.sendTempMessageToChannel(channel, "Bot '" + user.getName() + "': Ping: " + jda.getPing() + "ms" + " Status: " + jda.getStatus().name());

		} else if(msg.startsWith("!clear")) {
			message.delete().queue();
			final Member member = event.getMember();
			if(!member.hasPermission(Permission.MANAGE_ROLES)) {
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ Vous n'avez pas la permission d'utiliser !clear.");
			}
			final String[] args = message.getContentDisplay().split(" ");
			if(args.length < 2) {
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ Usage: !clear <all|number>");
				return;
			}

			if(args[1].equalsIgnoreCase("all")) {
				final List<Message> hist = channel.getHistoryBefore(message.getIdLong(), 100).complete().getRetrievedHistory();
				hist.stream().forEach(history -> history.delete().queue());
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ " + hist.size() + " messages ont été supprimés.");
			} else if(Matcher.isInt(args[1])) {
				final int i = Integer.parseInt(args[1]);
				if(i > 100 || i < 1) {
					EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ " + i + " doit être compris entre 1 et 100.");
					return;
				}
				final List<Message> hist = channel.getHistoryBefore(message.getIdLong(), i).complete().getRetrievedHistory();
				hist.stream().forEach(history -> history.delete().queue());
				EmeraldDiscord.sendTempMessageToChannel(channel, member.getAsMention() + " ➤ " + hist.size() + " messages ont été supprimés.");
			}
		}
	}

}

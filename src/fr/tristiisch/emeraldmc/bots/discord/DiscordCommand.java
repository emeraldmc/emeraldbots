package fr.tristiisch.emeraldmc.bots.discord;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.bungee.EmeraldBungee;
import fr.tristiisch.emeraldmc.api.bungee.utils.BungeeConfigUtils;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.bots.teamspeak.TeamspeakCommand;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class DiscordCommand extends Command {

	private static Map<UUID, Long> cooldown = new HashMap<>();

	public static void addPlayer(final ProxiedPlayer player) {
		cooldown.put(player.getUniqueId(), Utils.getCurrentTimeinSeconds() + 60);
	}

	public static boolean canUse(final ProxiedPlayer player) {
		if(cooldown.containsKey(player.getUniqueId())) {
			if(cooldown.get(player.getUniqueId()) < Utils.getCurrentTimeinSeconds()) {
				cooldown.remove(player.getUniqueId());
				return true;
			}
			return false;
		}
		return true;
	}

	public static void remove(final ProxiedPlayer player) {
		cooldown.remove(player.getUniqueId());
	}

	public DiscordCommand(final String command, final String... aliases) {
		super(command, null, aliases);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(final CommandSender sender, final String[] args) {
		ProxyServer.getInstance().getScheduler().runAsync(EmeraldBungee.getInstance(), () -> {
			EmeraldPlayer emeraldPlayer = null;
			if(sender instanceof ProxiedPlayer) {
				final ProxiedPlayer player = (ProxiedPlayer) sender;
				emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			}

			if(args.length == 0) {

				if(emeraldPlayer != null && !emeraldPlayer.getGroup().isStaffMember()) {
					TeamspeakCommand.sendTeamspeakIp((ProxiedPlayer) sender);
					return;
				}

				final TextComponent message = new TextComponent(Utils.color("&2EmeraldMC &7» &aDiscord &c&l&nprivé&a du staff: "));

				final TextComponent ip = new TextComponent(Utils.color("&2https://discord.gg/FZZmTn7"));
				ip.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&2Cliquez pour rejoindre le discord")).create()));
				ip.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/FZZmTn7"));
				message.addExtra(ip);
				sender.sendMessage(message);

			} else if(args[0].equalsIgnoreCase("stop")) {

				if(emeraldPlayer != null && emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
					sender.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
					return;
				}
				EmeraldDiscord.disconnect();
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &cDiscordBot deconnecté."));

			} else if(args[0].equalsIgnoreCase("start")) {
				if(emeraldPlayer != null && emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
					sender.sendMessage(BungeeConfigUtils.getString("commun.messages.noperm"));
					return;
				}
				EmeraldDiscord.connect();
				sender.sendMessage(Utils.color("&2EmeraldMC &7» &aDiscordBot connecté."));
			}
		});
	}
}

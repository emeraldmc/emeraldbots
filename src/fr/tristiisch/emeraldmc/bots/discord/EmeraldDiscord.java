
package fr.tristiisch.emeraldmc.bots.discord;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.security.auth.login.LoginException;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.bots.Main;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.managers.GuildController;
import net.md_5.bungee.api.ProxyServer;

public class EmeraldDiscord {

	public static JDA jda;

	public static void connect() {

		final JDABuilder builder = new JDABuilder(AccountType.BOT);

		builder.setToken("NDU3MjU3MTE4MDAxNzkxMDM5.DgWmHw.UIrUHaPwR7WOdRGzECY21wlZF4E");
		builder.setAutoReconnect(true);
		builder.setStatus(OnlineStatus.IDLE);

		builder.setGame(Game.playing("play.emeraldmc.fr"));

		builder.addEventListener(new DiscordListener());

		try {
			jda = builder.buildAsync();
			ProxyServer.getInstance().getScheduler().schedule(Main.getInstance(), () -> updateConnected(), 0, 1, TimeUnit.MINUTES);
		} catch(final LoginException e) {
			e.printStackTrace();
		}
	}

	public static void updateConnected() {
		final int co = ProxyServer.getInstance().getOnlineCount();
		final JDA jda = EmeraldDiscord.jda;
		jda.getPresence().setGame(Game.playing(co + " connecté" + Utils.withOrWithoutS(co) + " | play.emeraldmc.fr"));
	}

	public static void checkName(final Guild guild, final Member member) {
		final List<TextChannel> channels = guild.getTextChannelsByName("bot", false);
		TextChannel channel;
		if(!channels.isEmpty()) {
			channel = channels.get(0);
		} else {
			channel = guild.getDefaultChannel();
		}

		final GuildController guildController = guild.getController();

		final EmeraldPlayer emeraldPlayer = MySQL.getPlayer(member.getEffectiveName());
		if(emeraldPlayer == null) {
			guildController.removeRolesFromMember(member, member.getRoles()).queue();
			channel.sendMessage(member.getAsMention() + " ➤ " + member.getEffectiveName() + " n'est pas connu par EmeraldMC, merci d'utiliser le même pseudo que en jeux").queue();
			return;
		}
	}

	public static void setRole(final Member author, final Guild guild, final MessageChannel channel, final String memberName) {
		final List<Member> members = guild.getMembersByEffectiveName(memberName, false);

		if(members.isEmpty()) {
			EmeraldDiscord.sendTempMessageToChannel(channel, author.getAsMention() + " ➤ Le pseudo " + memberName + " est introuvable sur ce Discord.");
			return;
		}

		if(members.size() > 1) {
			EmeraldDiscord.sendTempMessageToChannel(channel,
					author.getAsMention() + " ➤ Le pseudo " + memberName + " est associé à plusieurs comptes: " + members.stream().map(member -> member.getEffectiveName()).collect(
							Collectors.joining(", ")) + ".");
			return;
		}

		final Member member = members.get(0);

		final EmeraldPlayer emeraldPlayer = MySQL.getPlayer(member.getEffectiveName());
		if(emeraldPlayer == null) {
			EmeraldDiscord.sendTempMessageToChannel(channel, author.getAsMention() + " ➤ Le pseudo " + member.getEffectiveName() + " est introuvable dans la base de donnés minecraft EmeraldMC.");
			return;
		}
		final GuildController guildController = guild.getController();

		final List<Role> roles = jda.getRolesByName(emeraldPlayer.getGroup().getName(), true);

		if(roles.isEmpty()) {
			EmeraldDiscord.sendTempMessageToChannel(channel, author.getAsMention() + " ➤ Le rôle " + emeraldPlayer.getGroup().getName() + " n'existe pas sur ce Discord.");
			return;
		}

		final Role role = roles.get(0);


		guildController.addSingleRoleToMember(member, role).queue();
		EmeraldDiscord.sendTempMessageToChannel(channel, author.getAsMention() + " ➤ " + member.getEffectiveName() + " a désormais le rôle " + role.getName());
	}

	public static void sendTempMessageToChannel(final MessageChannel channel, final String msg) {
		final Message out = new MessageBuilder(msg).build();
		channel.sendMessage(out).complete().delete().queueAfter(1, TimeUnit.MINUTES);
	}

	public static void disconnect() {
		if(jda != null) {
			jda.shutdown();
		}
	}
}
